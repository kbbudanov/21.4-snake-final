// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_MainPawn_generated_h
#error "MainPawn.generated.h already included, missing '#pragma once' in MainPawn.h"
#endif
#define SNAKE_MainPawn_generated_h

#define Snake_Source_Snake_MainPawn_h_19_SPARSE_DATA
#define Snake_Source_Snake_MainPawn_h_19_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define Snake_Source_Snake_MainPawn_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlePlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlePlayerVerticalInput);


#define Snake_Source_Snake_MainPawn_h_19_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAMainPawn(); \
	friend struct Z_Construct_UClass_AMainPawn_Statics; \
public: \
	DECLARE_CLASS(AMainPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AMainPawn)


#define Snake_Source_Snake_MainPawn_h_19_INCLASS \
private: \
	static void StaticRegisterNativesAMainPawn(); \
	friend struct Z_Construct_UClass_AMainPawn_Statics; \
public: \
	DECLARE_CLASS(AMainPawn, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AMainPawn)


#define Snake_Source_Snake_MainPawn_h_19_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AMainPawn(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AMainPawn) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainPawn); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainPawn(AMainPawn&&); \
	NO_API AMainPawn(const AMainPawn&); \
public:


#define Snake_Source_Snake_MainPawn_h_19_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AMainPawn(AMainPawn&&); \
	NO_API AMainPawn(const AMainPawn&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AMainPawn); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AMainPawn); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AMainPawn)


#define Snake_Source_Snake_MainPawn_h_19_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_Snake_MainPawn_h_16_PROLOG
#define Snake_Source_Snake_MainPawn_h_19_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_MainPawn_h_19_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_MainPawn_h_19_SPARSE_DATA \
	Snake_Source_Snake_MainPawn_h_19_RPC_WRAPPERS \
	Snake_Source_Snake_MainPawn_h_19_INCLASS \
	Snake_Source_Snake_MainPawn_h_19_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_MainPawn_h_19_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_MainPawn_h_19_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_MainPawn_h_19_SPARSE_DATA \
	Snake_Source_Snake_MainPawn_h_19_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_MainPawn_h_19_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_MainPawn_h_19_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class AMainPawn>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_MainPawn_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
