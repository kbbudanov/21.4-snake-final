// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/WorldGenerator.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWorldGenerator() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_AWorldGenerator_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_AWorldGenerator();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Snake();
	COREUOBJECT_API UClass* Z_Construct_UClass_UClass();
	SNAKE_API UClass* Z_Construct_UClass_AObstacles_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_AFloor_NoRegister();
// End Cross Module References
	void AWorldGenerator::StaticRegisterNativesAWorldGenerator()
	{
	}
	UClass* Z_Construct_UClass_AWorldGenerator_NoRegister()
	{
		return AWorldGenerator::StaticClass();
	}
	struct Z_Construct_UClass_AWorldGenerator_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObstacleSize_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_ObstacleSize;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObstacleClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_ObstacleClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_ObtacleElement_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_ObtacleElement_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_ObtacleElement;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloorClass_MetaData[];
#endif
		static const UE4CodeGen_Private::FClassPropertyParams NewProp_FloorClass;
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FloorElement_Inner;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FloorElement_MetaData[];
#endif
		static const UE4CodeGen_Private::FArrayPropertyParams NewProp_FloorElement;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AWorldGenerator_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWorldGenerator_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "WorldGenerator.h" },
		{ "ModuleRelativePath", "WorldGenerator.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObstacleSize_MetaData[] = {
		{ "ModuleRelativePath", "WorldGenerator.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObstacleSize = { "ObstacleSize", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWorldGenerator, ObstacleSize), METADATA_PARAMS(Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObstacleSize_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObstacleSize_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObstacleClass_MetaData[] = {
		{ "Category", "WorldGenerator" },
		{ "ModuleRelativePath", "WorldGenerator.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObstacleClass = { "ObstacleClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWorldGenerator, ObstacleClass), Z_Construct_UClass_AObstacles_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObstacleClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObstacleClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObtacleElement_Inner = { "ObtacleElement", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AObstacles_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObtacleElement_MetaData[] = {
		{ "ModuleRelativePath", "WorldGenerator.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObtacleElement = { "ObtacleElement", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWorldGenerator, ObtacleElement), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObtacleElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObtacleElement_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorClass_MetaData[] = {
		{ "Category", "WorldGenerator" },
		{ "ModuleRelativePath", "WorldGenerator.h" },
	};
#endif
	const UE4CodeGen_Private::FClassPropertyParams Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorClass = { "FloorClass", nullptr, (EPropertyFlags)0x0014000000010001, UE4CodeGen_Private::EPropertyGenFlags::Class, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWorldGenerator, FloorClass), Z_Construct_UClass_AFloor_NoRegister, Z_Construct_UClass_UClass, METADATA_PARAMS(Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorClass_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorClass_MetaData)) };
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorElement_Inner = { "FloorElement", nullptr, (EPropertyFlags)0x0000000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, 0, Z_Construct_UClass_AFloor_NoRegister, METADATA_PARAMS(nullptr, 0) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorElement_MetaData[] = {
		{ "ModuleRelativePath", "WorldGenerator.h" },
	};
#endif
	const UE4CodeGen_Private::FArrayPropertyParams Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorElement = { "FloorElement", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Array, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AWorldGenerator, FloorElement), EArrayPropertyFlags::None, METADATA_PARAMS(Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorElement_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorElement_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AWorldGenerator_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObstacleSize,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObstacleClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObtacleElement_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWorldGenerator_Statics::NewProp_ObtacleElement,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorClass,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorElement_Inner,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AWorldGenerator_Statics::NewProp_FloorElement,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AWorldGenerator_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AWorldGenerator>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AWorldGenerator_Statics::ClassParams = {
		&AWorldGenerator::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AWorldGenerator_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AWorldGenerator_Statics::PropPointers),
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AWorldGenerator_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AWorldGenerator_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AWorldGenerator()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AWorldGenerator_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AWorldGenerator, 2812995206);
	template<> SNAKE_API UClass* StaticClass<AWorldGenerator>()
	{
		return AWorldGenerator::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AWorldGenerator(Z_Construct_UClass_AWorldGenerator, &AWorldGenerator::StaticClass, TEXT("/Script/Snake"), TEXT("AWorldGenerator"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AWorldGenerator);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
