// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKE_WorldGenerator_generated_h
#error "WorldGenerator.generated.h already included, missing '#pragma once' in WorldGenerator.h"
#endif
#define SNAKE_WorldGenerator_generated_h

#define Snake_Source_Snake_WorldGenerator_h_20_SPARSE_DATA
#define Snake_Source_Snake_WorldGenerator_h_20_RPC_WRAPPERS
#define Snake_Source_Snake_WorldGenerator_h_20_RPC_WRAPPERS_NO_PURE_DECLS
#define Snake_Source_Snake_WorldGenerator_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWorldGenerator(); \
	friend struct Z_Construct_UClass_AWorldGenerator_Statics; \
public: \
	DECLARE_CLASS(AWorldGenerator, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AWorldGenerator)


#define Snake_Source_Snake_WorldGenerator_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAWorldGenerator(); \
	friend struct Z_Construct_UClass_AWorldGenerator_Statics; \
public: \
	DECLARE_CLASS(AWorldGenerator, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AWorldGenerator)


#define Snake_Source_Snake_WorldGenerator_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWorldGenerator(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWorldGenerator) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWorldGenerator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWorldGenerator); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWorldGenerator(AWorldGenerator&&); \
	NO_API AWorldGenerator(const AWorldGenerator&); \
public:


#define Snake_Source_Snake_WorldGenerator_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWorldGenerator(AWorldGenerator&&); \
	NO_API AWorldGenerator(const AWorldGenerator&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWorldGenerator); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWorldGenerator); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWorldGenerator)


#define Snake_Source_Snake_WorldGenerator_h_20_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_Snake_WorldGenerator_h_17_PROLOG
#define Snake_Source_Snake_WorldGenerator_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_WorldGenerator_h_20_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_WorldGenerator_h_20_SPARSE_DATA \
	Snake_Source_Snake_WorldGenerator_h_20_RPC_WRAPPERS \
	Snake_Source_Snake_WorldGenerator_h_20_INCLASS \
	Snake_Source_Snake_WorldGenerator_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_WorldGenerator_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_WorldGenerator_h_20_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_WorldGenerator_h_20_SPARSE_DATA \
	Snake_Source_Snake_WorldGenerator_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_WorldGenerator_h_20_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_WorldGenerator_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class AWorldGenerator>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_WorldGenerator_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
