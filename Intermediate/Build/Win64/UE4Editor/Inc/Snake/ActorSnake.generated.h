// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActorTail;
class AActor;
#ifdef SNAKE_ActorSnake_generated_h
#error "ActorSnake.generated.h already included, missing '#pragma once' in ActorSnake.h"
#endif
#define SNAKE_ActorSnake_generated_h

#define Snake_Source_Snake_ActorSnake_h_29_SPARSE_DATA
#define Snake_Source_Snake_ActorSnake_h_29_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execSnakeDestroyer); \
	DECLARE_FUNCTION(execSpeedUp); \
	DECLARE_FUNCTION(execSnakeElementOverlap); \
	DECLARE_FUNCTION(execMove); \
	DECLARE_FUNCTION(execAddSnakeElement);


#define Snake_Source_Snake_ActorSnake_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execSnakeDestroyer); \
	DECLARE_FUNCTION(execSpeedUp); \
	DECLARE_FUNCTION(execSnakeElementOverlap); \
	DECLARE_FUNCTION(execMove); \
	DECLARE_FUNCTION(execAddSnakeElement);


#define Snake_Source_Snake_ActorSnake_h_29_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAActorSnake(); \
	friend struct Z_Construct_UClass_AActorSnake_Statics; \
public: \
	DECLARE_CLASS(AActorSnake, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AActorSnake)


#define Snake_Source_Snake_ActorSnake_h_29_INCLASS \
private: \
	static void StaticRegisterNativesAActorSnake(); \
	friend struct Z_Construct_UClass_AActorSnake_Statics; \
public: \
	DECLARE_CLASS(AActorSnake, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AActorSnake)


#define Snake_Source_Snake_ActorSnake_h_29_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AActorSnake(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AActorSnake) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AActorSnake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AActorSnake); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AActorSnake(AActorSnake&&); \
	NO_API AActorSnake(const AActorSnake&); \
public:


#define Snake_Source_Snake_ActorSnake_h_29_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AActorSnake(AActorSnake&&); \
	NO_API AActorSnake(const AActorSnake&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AActorSnake); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AActorSnake); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AActorSnake)


#define Snake_Source_Snake_ActorSnake_h_29_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__isStarted() { return STRUCT_OFFSET(AActorSnake, isStarted); } \
	FORCEINLINE static uint32 __PPO__PrevLockation() { return STRUCT_OFFSET(AActorSnake, PrevLockation); }


#define Snake_Source_Snake_ActorSnake_h_26_PROLOG
#define Snake_Source_Snake_ActorSnake_h_29_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_ActorSnake_h_29_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_ActorSnake_h_29_SPARSE_DATA \
	Snake_Source_Snake_ActorSnake_h_29_RPC_WRAPPERS \
	Snake_Source_Snake_ActorSnake_h_29_INCLASS \
	Snake_Source_Snake_ActorSnake_h_29_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_ActorSnake_h_29_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_ActorSnake_h_29_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_ActorSnake_h_29_SPARSE_DATA \
	Snake_Source_Snake_ActorSnake_h_29_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_ActorSnake_h_29_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_ActorSnake_h_29_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class AActorSnake>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_ActorSnake_h


#define FOREACH_ENUM_EMOVEMENTDIRECTION(op) \
	op(EMovementDirection::UP) \
	op(EMovementDirection::DOWN) \
	op(EMovementDirection::LEFT) \
	op(EMovementDirection::RIGHT) 

enum class EMovementDirection;
template<> SNAKE_API UEnum* StaticEnum<EMovementDirection>();

PRAGMA_ENABLE_DEPRECATION_WARNINGS
