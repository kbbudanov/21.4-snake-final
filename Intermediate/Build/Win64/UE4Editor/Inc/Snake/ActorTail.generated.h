// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef SNAKE_ActorTail_generated_h
#error "ActorTail.generated.h already included, missing '#pragma once' in ActorTail.h"
#endif
#define SNAKE_ActorTail_generated_h

#define Snake_Source_Snake_ActorTail_h_18_SPARSE_DATA
#define Snake_Source_Snake_ActorTail_h_18_RPC_WRAPPERS \
	virtual void SetFirstElementType_Implementation(); \
 \
	DECLARE_FUNCTION(execToggleCollision); \
	DECLARE_FUNCTION(execHandleBeginOverlap); \
	DECLARE_FUNCTION(execSetFirstElementType);


#define Snake_Source_Snake_ActorTail_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execToggleCollision); \
	DECLARE_FUNCTION(execHandleBeginOverlap); \
	DECLARE_FUNCTION(execSetFirstElementType);


#define Snake_Source_Snake_ActorTail_h_18_EVENT_PARMS
#define Snake_Source_Snake_ActorTail_h_18_CALLBACK_WRAPPERS
#define Snake_Source_Snake_ActorTail_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAActorTail(); \
	friend struct Z_Construct_UClass_AActorTail_Statics; \
public: \
	DECLARE_CLASS(AActorTail, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AActorTail) \
	virtual UObject* _getUObject() const override { return const_cast<AActorTail*>(this); }


#define Snake_Source_Snake_ActorTail_h_18_INCLASS \
private: \
	static void StaticRegisterNativesAActorTail(); \
	friend struct Z_Construct_UClass_AActorTail_Statics; \
public: \
	DECLARE_CLASS(AActorTail, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Snake"), NO_API) \
	DECLARE_SERIALIZER(AActorTail) \
	virtual UObject* _getUObject() const override { return const_cast<AActorTail*>(this); }


#define Snake_Source_Snake_ActorTail_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AActorTail(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AActorTail) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AActorTail); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AActorTail); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AActorTail(AActorTail&&); \
	NO_API AActorTail(const AActorTail&); \
public:


#define Snake_Source_Snake_ActorTail_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AActorTail(AActorTail&&); \
	NO_API AActorTail(const AActorTail&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AActorTail); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AActorTail); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AActorTail)


#define Snake_Source_Snake_ActorTail_h_18_PRIVATE_PROPERTY_OFFSET
#define Snake_Source_Snake_ActorTail_h_15_PROLOG \
	Snake_Source_Snake_ActorTail_h_18_EVENT_PARMS


#define Snake_Source_Snake_ActorTail_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_ActorTail_h_18_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_ActorTail_h_18_SPARSE_DATA \
	Snake_Source_Snake_ActorTail_h_18_RPC_WRAPPERS \
	Snake_Source_Snake_ActorTail_h_18_CALLBACK_WRAPPERS \
	Snake_Source_Snake_ActorTail_h_18_INCLASS \
	Snake_Source_Snake_ActorTail_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Snake_Source_Snake_ActorTail_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Snake_Source_Snake_ActorTail_h_18_PRIVATE_PROPERTY_OFFSET \
	Snake_Source_Snake_ActorTail_h_18_SPARSE_DATA \
	Snake_Source_Snake_ActorTail_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	Snake_Source_Snake_ActorTail_h_18_CALLBACK_WRAPPERS \
	Snake_Source_Snake_ActorTail_h_18_INCLASS_NO_PURE_DECLS \
	Snake_Source_Snake_ActorTail_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKE_API UClass* StaticClass<class AActorTail>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Snake_Source_Snake_ActorTail_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
