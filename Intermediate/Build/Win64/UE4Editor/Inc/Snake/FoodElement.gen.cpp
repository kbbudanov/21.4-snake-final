// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/FoodElement.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFoodElement() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_AFoodElement_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_AFoodElement();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Snake();
	ENGINE_API UClass* Z_Construct_UClass_UStaticMeshComponent_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_AFood_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_UiInteractable_NoRegister();
// End Cross Module References
	void AFoodElement::StaticRegisterNativesAFoodElement()
	{
	}
	UClass* Z_Construct_UClass_AFoodElement_NoRegister()
	{
		return AFoodElement::StaticClass();
	}
	struct Z_Construct_UClass_AFoodElement_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_MeshComponent_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_MeshComponent;
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_FoodMain_MetaData[];
#endif
		static const UE4CodeGen_Private::FObjectPropertyParams NewProp_FoodMain;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFoodElement_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodElement_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "FoodElement.h" },
		{ "ModuleRelativePath", "FoodElement.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodElement_Statics::NewProp_MeshComponent_MetaData[] = {
		{ "Category", "FoodElement" },
		{ "EditInline", "true" },
		{ "ModuleRelativePath", "FoodElement.h" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFoodElement_Statics::NewProp_MeshComponent = { "MeshComponent", nullptr, (EPropertyFlags)0x00100000000a001d, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodElement, MeshComponent), Z_Construct_UClass_UStaticMeshComponent_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFoodElement_Statics::NewProp_MeshComponent_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodElement_Statics::NewProp_MeshComponent_MetaData)) };
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodElement_Statics::NewProp_FoodMain_MetaData[] = {
		{ "Comment", "//UPROPERTY(EditDefaultsOnly)\n//TSubclassOf<AFood> FoodMainClass;\n" },
		{ "ModuleRelativePath", "FoodElement.h" },
		{ "ToolTip", "UPROPERTY(EditDefaultsOnly)\nTSubclassOf<AFood> FoodMainClass;" },
	};
#endif
	const UE4CodeGen_Private::FObjectPropertyParams Z_Construct_UClass_AFoodElement_Statics::NewProp_FoodMain = { "FoodMain", nullptr, (EPropertyFlags)0x0010000000000000, UE4CodeGen_Private::EPropertyGenFlags::Object, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(AFoodElement, FoodMain), Z_Construct_UClass_AFood_NoRegister, METADATA_PARAMS(Z_Construct_UClass_AFoodElement_Statics::NewProp_FoodMain_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodElement_Statics::NewProp_FoodMain_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_AFoodElement_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodElement_Statics::NewProp_MeshComponent,
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_AFoodElement_Statics::NewProp_FoodMain,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_AFoodElement_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UiInteractable_NoRegister, (int32)VTABLE_OFFSET(AFoodElement, IiInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFoodElement_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFoodElement>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFoodElement_Statics::ClassParams = {
		&AFoodElement::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_AFoodElement_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_AFoodElement_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFoodElement_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodElement_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFoodElement()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFoodElement_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFoodElement, 3513625411);
	template<> SNAKE_API UClass* StaticClass<AFoodElement>()
	{
		return AFoodElement::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFoodElement(Z_Construct_UClass_AFoodElement, &AFoodElement::StaticClass, TEXT("/Script/Snake"), TEXT("AFoodElement"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFoodElement);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
