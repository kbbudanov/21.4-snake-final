// Fill out your copyright notice in the Description page of Project Settings.

#include "ActorSnake.h"
#include "ActorTail.h"

// Sets default values
AActorTail::AActorTail()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("MeshComponent");
	RootComponent = MeshComponent;
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);


}

// Called when the game starts or when spawned
void AActorTail::BeginPlay()
{
	Super::BeginPlay();
	
	
}

// Called every frame
void AActorTail::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AActorTail::SetFirstElementType_Implementation()
{
	MeshComponent->OnComponentBeginOverlap.AddDynamic(this, &AActorTail::HandleBeginOverlap);
}

void AActorTail::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<AActorSnake>(Interactor);
	if (IsValid(Snake))
		Snake->SnakeDestroyer();
	UE_LOG(LogTemp, Warning, TEXT("Deth by tail"));
}

void AActorTail::HandleBeginOverlap(UPrimitiveComponent* OverlappedComponent, 
									AActor* OtherActor, 
									UPrimitiveComponent* OtherComponent, 
									int32 OtherBodyIndex, 
									bool bFromSweep, 
									const FHitResult& SweepResult)
{
	if (IsValid(SnakeOwner))
	{
		SnakeOwner->SnakeElementOverlap(this,OtherActor);
	}

}

void AActorTail::ToggleCollision()
{
	if (MeshComponent->GetCollisionEnabled() == ECollisionEnabled::NoCollision)
	{
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	else {
		MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}
}

