// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	UE_LOG(LogTemp, Warning, TEXT("%s"), *GetName());
	Super::BeginPlay();
	CreateFood();
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AFood::CreateFood()
{
	
	
	int randIndeX(0), randIndeY(0);
	float ElSize = 70.f;
	randIndeY = FMath::RandRange(2, massSize-2);
	while (!(randIndeX % 2))
	{
		randIndeX = FMath::RandRange(2, massSize*2);
	}
	FVector NewLocation(randIndeY * ElSize, randIndeX * ElSize, 0);
	FTransform NewTransform(NewLocation);

	AFoodElement* NewFoodElem = GetWorld()->SpawnActor<AFoodElement>(FoodElementClass, NewTransform);
	NewFoodElem->FoodMain = this;
	isBonusSpawned = true;
   
}

void AFood::isBonus()
{
	isBonusSpawned = false;
}



