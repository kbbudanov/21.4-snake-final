// Fill out your copyright notice in the Description page of Project Settings.


#include "Head.h"

// Sets default values
AHead::AHead()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Head_StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("SnakesHeadMesh"); //��������� ������ ���
	RootComponent = CreateDefaultSubobject<USceneComponent>("Root");		// ��������� ��� ��� ����� ����
	Head_StaticMesh->SetupAttachment(RootComponent); // �������������� ��� � ����

	PawnMovementComponent = CreateDefaultSubobject<UFloatingPawnMovement>("PawnMovement");
}

// Called when the game starts or when spawned
void AHead::BeginPlay()
{
	Super::BeginPlay();
	
}

void AHead::MoveForvardBack(float Axis)
{
	UE_LOG(LogTemp, Warning, TEXT("MoveForwardBack %f"), Axis);
	AddMovementInput(GetActorForwardVector(), Axis, false);
}

void AHead::MoveRightLeft(float Axis)
{
	UE_LOG(LogTemp, Warning, TEXT("MoveRightLeft % f"), Axis);
	AddMovementInput(GetActorRightVector(), Axis, false);
}

// Called every frame
void AHead::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AHead::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForwardBack", this, &AHead::MoveForvardBack);
	PlayerInputComponent->BindAxis("MoveRightLeft", this, &AHead::MoveRightLeft);

}

