// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldGenerator.h"

// Sets default values
AWorldGenerator::AWorldGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
}

// Called when the game starts or when spawned
void AWorldGenerator::BeginPlay()
{
	Super::BeginPlay();
    ObstacleSize = 70.f;
    GeneratorField();
}


// Called every frame
void AWorldGenerator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWorldGenerator::GeneratorField()
{
   

    //������ �����
    for (int x = -2; x <= 2*massSize; x++) 
    {
        for (int y = -2; y <= (massSize+2); y++)
        {
            if (y == -2 || x == -2 || y == (massSize+2) || x == 2*massSize)
            {
                PlaceObstacleElement(y, x, 0, ObstacleSize);
            }
            PlaceFloorElement(y, x, -ObstacleSize*(FMath::RandRange(15, 20))/10, ObstacleSize);
        }
    }
    //��������� ������������� ����
    for (int y = 0; y < massSize*2; y++)
    {
       
       if (!(y % 2))
        {
            for (int x = 0; x < (int)(massSize/1.5); x++)
            {

                PlaceObstacleElement(FMath::RandRange(0, massSize), y, 0, ObstacleSize);

                
            }

        }
     }

}

void AWorldGenerator::PlaceObstacleElement(int32 x, int32 y, float z, float ObstSize)
{

    FVector NewLocation(FVector(x * ObstacleSize, y * ObstacleSize, z));
    FTransform NewTransform(NewLocation); // ���������� ������ ��������
    auto NewObtacleElement = GetWorld()->SpawnActor<AObstacles>(ObstacleClass, NewTransform);
    ObtacleElement.Add(NewObtacleElement);
}

void AWorldGenerator::PlaceFloorElement(int32 x, int32 y, float z, float ObstSize)
{
    FVector NewLocation(FVector(x * ObstacleSize, y * ObstacleSize, z));
    FTransform NewTransform(NewLocation); // ���������� ������ ��������
    auto NewFloorElement = GetWorld()->SpawnActor<AFloor>(FloorClass, NewTransform);
    FloorElement.Add(NewFloorElement);
}
