// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "iInteractable.h"
#include "ActorSnake.h"
#include "FoodElement.h"
#include "Math/UnrealMathUtility.h"

#include "Food.generated.h"

UCLASS()
class SNAKE_API AFood : public AActor, public IiInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	
	
	UPROPERTY(VisibleAnywhere)
		TSubclassOf<AFoodElement> FoodElementClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		AFoodElement* FoodElem;




protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	bool isBonusSpawned = false;
	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//UFUNCTION()
	UFUNCTION(BlueprintCallable)
	void CreateFood();
	UFUNCTION(BlueprintCallable)
	void isBonus();

};
