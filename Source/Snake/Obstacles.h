// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "iInteractable.h"


#include "Components/StaticMeshComponent.h"
#include "Obstacles.generated.h"

UCLASS()
class SNAKE_API AObstacles : public AActor, public IiInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObstacles();
		//���� �����������
	UPROPERTY(VisibleAnywhere, Category = "Component")
		UStaticMeshComponent* Obstacles_StaticMesh;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void Interact(AActor* Interactor, bool bIsHead) override;


};
