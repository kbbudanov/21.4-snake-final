// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "GameFramework/Pawn.h"
#include "Head.generated.h"

UCLASS()
class SNAKE_API AHead : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AHead();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	void MoveForvardBack(float Axis);
	
	void MoveRightLeft(float Axis);

	


public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category="Component")
	class UStaticMeshComponent* Head_StaticMesh;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = "Component")
		class UFloatingPawnMovement* PawnMovementComponent;

};
